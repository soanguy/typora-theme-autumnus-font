# Typora-theme-autumnus-font

## Noto
Noto Sans CJK 和 Noto Serif CJK 文件太大，无法上传，可自行搜索下载。

## 悠哉字体
1. https://github.com/lxgw/yozai-font
2. https://www.maoken.com/freefonts/5423.html

## 其他字体安装

下载该仓库，将其中的字体文件安装即可。

1. 右击字体文件，点击安装即可。
2. 或者将字体文件复制到 C:\Windows\Fonts 目录下。